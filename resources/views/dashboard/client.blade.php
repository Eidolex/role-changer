<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>name</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($clients as $client)
            <tr>
                <td>{{$client->id}}</td>
                <td>{{$client->name}}</td>
                <td><button onclick="change({{$client->id}})">Swtich</button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <form id="swtichForm" action="{{route('dashboard.client-switch')}}" method="post">
        @csrf
        <input type="hidden" name="id" id="inputId">
    </form>
    <script>
        const swtichForm = document.getElementById("swtichForm");
        const inputId = document.getElementById("inputId");

        function change(id) {
            inputId.value = id;
            swtichForm.submit();
        }
    </script>
</body>

</html>