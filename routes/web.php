<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('flush', function () {
    session()->flush();
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard.index');

    // admin only route group
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::get('/agent', 'DashboardController@agent')->name('dashboard.agent');
        Route::post('/agent', 'DashboardController@switchAgent')->name('dashboard.agent-switch');
    });

    // admin, agent route group
    Route::group(['middleware' => 'auth:admin,agent'], function () {
        Route::get('/client', 'DashboardController@client')->name('dashboard.client');
        Route::post('/client', 'DashboardController@switchClient')->name('dashboard.client-switch');
        Route::get('/swtich-back', 'DashboardController@switchBack');
    });
});
Route::get('/login', 'TestController@login')->name('login');
