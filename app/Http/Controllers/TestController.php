<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        dd($id);
    }

    public function login()
    {
        Auth::loginUsingId(1);
    }
}
