<?php

namespace App\Http\Controllers;

use App;
use App\Client;
use App\Agent;
use Auth;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function callRoleMethod(array $roles, $params = [])
    {
        $user = Auth::user();
        $roles = array_map('strtolower', $roles);
        $role = 'Admin';

        if ($user instanceof Client) {
            $role = 'Client';
        }
        if ($user instanceof Agent) {
            $role = 'Agent';
        }

        if (!in_array(strtolower($role), $roles)) {
            return abort(404);
        }

        list(
            'class' => $class,
            'function' => $method
        ) = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2)[1];

        $class = str_replace('App\Http\Controllers', "App\Http\Controllers\\$role", $class);

        return App::call("{$class}@{$method}", $params);
    }
}
