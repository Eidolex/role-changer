<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Client;
use App\Services\RoleSwtichService;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * @var RoleSwtichService
     */
    protected $roleSwtichService;

    public function __construct(RoleSwtichService $roleSwtichService)
    {
        $this->roleSwtichService = $roleSwtichService;
    }

    public function index()
    {
        return $this->callRoleMethod(['admin', 'agent', 'client']);
    }

    public function agent()
    {
        $agents = Agent::all();

        return response()->view('dashboard.agent', compact('agents'));
    }

    public function switchAgent(Request $request)
    {
        if (!$request->has('id')) {
            return redirect()->route('dashboard.agent');
        }
        $this->roleSwtichService->switch('agent', $request->input('id'));
        return redirect()->route('dashboard.index');
    }

    public function client()
    {
        $clients = Client::all();

        return response()->view('dashboard.client', compact('clients'));
    }

    public function switchClient(Request $request)
    {
        if (!$request->has('id')) {
            return redirect()->route('dashboard.client');
        }
        $this->roleSwtichService->switch('client', $request->input('id'));
        return redirect()->route('dashboard.index');
    }

    public function switchBack()
    {
        $this->roleSwtichService->swtichBack();
        return redirect()->route('dashboard.index');
    }
}
