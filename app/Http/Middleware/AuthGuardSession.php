<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class AuthGuardSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guard = $this->getGuard();
        if ($guard) {
            config([
                'auth.defaults.guard' => $guard
            ]);
        }
        return $next($request);
    }

    protected function getGuard()
    {
        return Session::get('guard');
    }
}
