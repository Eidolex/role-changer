<?php

namespace App\Services;

use Auth;

class RoleSwtichService
{

    protected $swtichableRoles = [];

    public function __construct()
    {
        $this->initializeSwitchableRoles();
    }

    public function switch($role, $id)
    {
        if (!in_array($role, $this->swtichableRoles)) {
            throw new \Exception("Role : {$role} is not swtichable");
        }

        session()->put('guard', $role);
        $user = Auth::guard($role)->loginUsingId($id);

        if (!$user) {
            throw new \Exception("Role swtiching went wrong");
        }
    }

    public function swtichBack()
    {
        foreach ($this->swtichableRoles as $guard) {
            auth($guard)->logout();
        }
        session()->forget('guard');
    }

    private function initializeSwitchableRoles()
    {
        $defaultGuard = config('auth.defaults.guard');

        $guards = config('auth.guards');

        foreach ($guards as $key => $guard) {
            if ($key === $defaultGuard || $guard['driver'] !== 'session') continue;
            $this->swtichableRoles[] = $key;
        }
    }
}
